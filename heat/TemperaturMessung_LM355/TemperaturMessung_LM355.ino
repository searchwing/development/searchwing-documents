void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A0, INPUT);
}

void loop() {
  int result = analogRead(A0);                        //Einlesen der Spannung im bereich von 0 - 1023
  float voltage = (5.0/1023) * (float)result;         //eingelesenen wert in Spannung umrechnen
  float temp = 1 / (0.010001678 / voltage) - 273.15;      //spannung mit ermitteltem Wert in Temperatur umrechnen
  //float temp = 1 / (0.01 / voltage) - 273.15;
  Serial.print(voltage);
  Serial.print(", ");
  Serial.print(temp);
  Serial.println();
  delay(1000);                                        //1 sekunde warten
}
